/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import ShapeDrawableComponent from './src/component/ShapeDrawableComponent';

const App = () => {
  return (
    <ShapeDrawableComponent />
  )
};

export default App;

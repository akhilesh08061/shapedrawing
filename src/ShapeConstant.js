export const CIRCLE = "circle"
export const TRIANGLE = "triangle"
export const OVAL = "oval"
export const RECTANGLE = "rectangle"
export const SQUARE = "square"


//Clicked view constants
export const CIRCLE_VIEW_CLICKED = "circle_view_clicked"
export const TRIANGLE_VIEW_CLICKED = "triangle_view_clicked"
export const OVAL_VIEW_CLICKED = "oval_view_clicked"
export const RECTANGLE_VIEW_CLICKED = "rectangle_view_clicked"
export const SQUARE_VIEW_CLICKED = "square_view_clicked"

//import React in our code
import React, { useState } from 'react';
//import all the components we are going to use
import { StyleSheet, View, } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import * as Colors from '../ColorConstant';
import * as Shapes from '../ShapeConstant';

//Custom component where various shapes are getting drawn.
const ShapeDrawableComponent = () => {

    //initial state declaration
    const [circleShapeColor, setCircleShapeColor] = useState(Colors.TRANSPARENT);
    const [triangleShapeColor, setTriangleShapeColor] = useState(Colors.GRAY);
    const [OvalShapeColor, setOvalShapeColor] = useState(Colors.TRANSPARENT);
    const [rectangleShapeColor, setRectangleShapeColor] = useState(Colors.TRANSPARENT);
    const [squareShapeColor, setSquareShapeColor] = useState(Colors.TRANSPARENT);
    const [selectedColor, setSelectedColor] = useState(Colors.RED);
    const [selectedShape, setSelectedShape] = useState(Shapes.CIRCLE);

    //set the color state of a perticular shape was being clicked.
    const setColor = (clickedView) => {
        if (selectedShape == Shapes.CIRCLE
            && clickedView == Shapes.CIRCLE_VIEW_CLICKED) {
            setCircleShapeColor(selectedColor)
        }
        else if (selectedShape == Shapes.RECTANGLE
            && clickedView == Shapes.RECTANGLE_VIEW_CLICKED) {
            setRectangleShapeColor(selectedColor)
        }
        else if (selectedShape == Shapes.TRIANGLE
            && clickedView == Shapes.TRIANGLE_VIEW_CLICKED) {
            setTriangleShapeColor(selectedColor)
        }
        else if (selectedShape == Shapes.OVAL
            && clickedView == Shapes.OVAL_VIEW_CLICKED) {
            setOvalShapeColor(selectedColor)
        }
        else if (selectedShape == Shapes.SQUARE
            && clickedView == Shapes.SQUARE_VIEW_CLICKED) {
            setSquareShapeColor(selectedColor)
        }
    }


    return (
        <View style={styles.container}>

            {/* shape drawing view */}
            <View style={styles.subContainer}>
                {/* View having circle and triangle shapes */}
                <View style={styles.shapeDrawableContainer}>
                    <View onStartShouldSetResponder={() => setColor(Shapes.CIRCLE_VIEW_CLICKED)} style={[styles.CircleShapeView, { backgroundColor: circleShapeColor }]} />
                    <View onStartShouldSetResponder={() => setColor(Shapes.TRIANGLE_VIEW_CLICKED)} style={[styles.TriangleShapeView, { borderBottomColor: triangleShapeColor }]} />
                </View>

                {/* View having oval and rectangle shapes */}
                <View style={styles.shapeDrawableContainer}>
                    <View onStartShouldSetResponder={() => setColor(Shapes.OVAL_VIEW_CLICKED)} style={[styles.OvalShapeView, { backgroundColor: OvalShapeColor }]} />
                    <View onStartShouldSetResponder={() => setColor(Shapes.RECTANGLE_VIEW_CLICKED)} style={[styles.RectangleShapeView, { backgroundColor: rectangleShapeColor }]} />
                </View>


                {/* View having square shapes */}
                <View style={styles.shapeDrawableContainer}>
                    <View onStartShouldSetResponder={() => setColor(Shapes.SQUARE_VIEW_CLICKED)} style={[styles.SquareShapeView, { backgroundColor: squareShapeColor }]} />
                </View>
            </View>

            {/* selection of color and shape picker view */}

            {/* dropdown item for color selection */}
            <View style={styles.pickerHolderView}>
                <Picker
                    selectedValue={selectedColor}
                    style={styles.colorPickerStyle}
                    onValueChange={(colorValue) =>
                        setSelectedColor(colorValue)
                    }>
                    <Picker.Item label="red" value={Colors.RED} />
                    <Picker.Item label="blue" value={Colors.BLUE} />
                    <Picker.Item label="teal" value={Colors.TEAL} />
                    <Picker.Item label="sky_blue" value={Colors.SKY_BLUE} />
                    <Picker.Item label="yellow" value={Colors.YELLOW} />
                    <Picker.Item label="orange" value={Colors.ORANGE} />
                </Picker>

                {/* dropdown item for shape selection */}
                <Picker
                    selectedValue={selectedShape}
                    style={styles.shapePickerStyle}
                    onValueChange={(shapeValue) =>
                        setSelectedShape(shapeValue)
                    }>
                    <Picker.Item label="circle" value={Shapes.CIRCLE} />
                    <Picker.Item label="triangle" value={Shapes.TRIANGLE} />
                    <Picker.Item label="oval" value={Shapes.OVAL} />
                    <Picker.Item label="rectangle" value={Shapes.RECTANGLE} />
                    <Picker.Item label="square" value={Shapes.SQUARE} />
                </Picker>

            </View>


        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subContainer: {
        flex: 1,
        borderColor: '#000',
        borderWidth: 1
    },

    pickerHolderView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginStart: '5%',
        marginEnd: '5%'
    },

    colorPickerStyle: {
        height: 80,
        width: 120
    },

    shapePickerStyle: {
        height: 80,
        width: 140
    },

    shapeDrawableContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginStart: '4%',
        marginEnd: '4%',
        marginTop: '10%'
    },
    CircleShapeView: {
        //To make Circle Shape
        width: 150,
        height: 150,
        borderRadius: 150 / 2,
        borderColor: '#000',
        borderWidth: 1,
    },
    OvalShapeView: {
        //To make Oval Shape
        marginTop: 20,
        marginStart: 30,
        width: 90,
        height: 90,
        borderRadius: 50,
        transform: [{ scaleX: 2 }],
        borderColor: '#000',
        borderWidth: 1
    },
    SquareShapeView: {
        //To make Square Shape
        width: 100,
        height: 100,
        borderColor: '#000',
        borderWidth: 1
    },
    RectangleShapeView: {
        //To make Rectangle Shape
        marginTop: 20,
        width: 100 * 2,
        height: 100,
        borderColor: '#000',
        borderWidth: 1,
    },
    TriangleShapeView: {
        //To make Triangle Shape
        width: 0,
        height: 0,
        borderLeftWidth: 60,
        borderRightWidth: 60,
        borderBottomWidth: 120,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
    },
});

export default ShapeDrawableComponent;
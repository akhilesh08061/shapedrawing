export const RED = "#EC0F20"
export const BLUE = "#0000FF"
export const TEAL = "#008080"
export const SKY_BLUE = "#B9DDF3"
export const YELLOW = "#FFE012"
export const ORANGE = "#DD5A13"
export const GRAY = "#e6e4e4"
export const TRANSPARENT = 'transparent'
